##daniel serrano
#
#
#
#
# Hash table
# have scourred the internet and textbooks to come up with the implementation
# uses list method to avoid collision

#hastable class will have a constructor, get hash algorithm, add,get,and delete methods

class HashTable:
        ##constructor will create a size of 17 elements in the table for this demonstration
	def __init__(self):
		self.size = 17
                ##the table will be set to None and be multipled by itseld
		self.table = [None] * self.size

	##hash function will use ASCII conversion to change chars into integer values	
	def _get_hash(self, key):
                ##set hash to zero
		hash = 0

		#for the char in string(key)
		for char in str(key):
                        #hash+= ord(char) to cahnge char value into unicode
			hash += ord(char)
			
		#return hash modulo self.size(table size)	
		return hash % self.size

	##add function will take self, key, and value to utilize hashing algorithm
	    ## to take a string as a key go through the hashing algorithm and place it in the correct index
	def add(self, key, value):
                ##key hash will be set to self and stored in the key
		key_hash = self._get_hash(key)

		##value will be paired to unique key
		key_value = [key, value]

		
		if self.table[key_hash] is None:
			self.table[key_hash] = list([key_value])
			return True
		else:
			for pair in self.table[key_hash]:
				if pair[0] == key:
					pair[1] = value
					return True
			self.table[key_hash].append(key_value)
			return True
			
	def get(self, key):
                ##key hash will return pair 
		key_hash = self._get_hash(key)
		if self.table[key_hash] is not None:
                        ##for pair in table if pair is equal to key return pair
			for pair in self.table[key_hash]:
				if pair[0] == key:
					return pair[1]
		return None
			
	def delete(self, key):
		key_hash = self._get_hash(key)
		
		if self.table[key_hash] is None:
			return False
		    ##for loop to determine length of table by using keyhash
		    ##if key is found pop from the 'list' if not found return false
		for i in range (0, len(self.table[key_hash])):
			if self.table[key_hash][i][0] == key:
				self.table[key_hash].pop(i)
				return True
		return False
			
	def print(self):
            ##print contents as long as there are items in table
		print('---PHONEBOOK----')
		for item in self.table:
			if item is not None:
				print(str(item))
			
h = HashTable()
h.add('Bob', '567-8888')
h.add('Ming', '293-6753')
h.add('Ming', '333-8233')
h.add('Ankit', '293-8625')
h.add('Aditya', '852-6551')
h.add('Alicia', '632-4123')
h.add('Mike', '567-2188')
h.add('Aditya', '777-8888')
h.add('Mike', '567-2188')
h.add('Rob', '567-2138')
h.add('Trip', '547-2588')
h.add('Cliff', '393-5176')
h.add('Jenny', '867-5309')
h.add('Ben', '555-3188')
h.add('Sam', '532-3418')
h.add('Travis', '672-2128')
h.add('Scott', '375-2189')
h.add('John', '562-3589')
h.add('Sabrina', '567-8901')
h.add('Elsbeth', '123-4567')

h.print()
h.delete('Bob')
h.print()

		
