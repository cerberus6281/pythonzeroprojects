# # # # # # # # # # # # # # # # # # # 
#zoo.py
#
# Author: Daniel Serrano
# Date:   August 5 2017#
# # # # # # # # # # # # # # # # # # #
#Note IDLE will print the objects

#import modules
import tkinter, animals
from tkinter import *


#added generic sounds incase you are unaable to hear the sounds

#Program a Zoo. The Zoo is a Python list containing Animal objects. A Parent class will need to be created called Animal.
#Your Animal parent class will need to encapsulate the following:
#Attributes such as animalType (string), age (number), color (string). These should be attributes all animals have.
#A stub function called makeNoise().
#The __init__ function.
#Build a __str__ function that will allow you to easily print each object. Calling print for the object will automatically call the __str__ function.
#You will need to create at least three subclasses (child classes) of Animal. For example, you could create subclasses called Birds, Snakes, Dogs, Cats, or anything else that would create a good group with attributes of the parent along with unique attributes. Each subclass will need an __init__ function that calls the parent __init__ function.

#You will need to create 2 instances of each subclass and print out the attributes associated with each.

#Be sure to use comments for both structure of the program and documentation of the code.

#All code must completely be your own individual work product.

#create objects for mammal, monkey, elephant, and goat
mammal = animals.Mammal('Regular animal', '10', 'rainbow')
monkey = animals.Monkey('Monkey','27', 'black')
elephant = animals.Elephant('Elephant','47', '50 shades of grey')
goat = animals.Goat('Goat','7', 'white')


#create class for gooey
class MyGUI:
    def __init__(self):
 
        # Create the main window.
        self.main_window = tkinter.Tk()
        #set labels 
        self.label1 = tkinter.Label(self.main_window,\
                                    text = 'Welcome to the Zoo Click on a animal name ---->')
        self.label2 = tkinter.Label(self.main_window,\
                                    text = 'I dare you' )
        mon = PhotoImage(file = "monkey.gif")
        self.label3 = tkinter.Label(self.main_window,\
                                    image = mon)
        ele = PhotoImage(file = "elephant.gif")
        self.label4 = tkinter.Label(self.main_window,\
                                    image = ele)
        go = PhotoImage(file = "goat.gif")
        self.label5 = tkinter.Label(self.main_window,\
                                    image = go)
        #set labels to sides of window
        self.label1.pack(side = 'top')
        self.label2.pack(side = 'bottom')
        self.label3.pack(side = 'left')
        self.label4.pack(side = 'left')
        self.label5.pack(side = 'left')
        # Create two frames. One for the Radiobuttons
        # and another for the regular Button widgets.
        self.top_frame = tkinter.Frame(self.main_window)
        self.bottom_frame = tkinter.Frame(self.main_window)
        
        # Create an IntVar object to use with
        # the Radiobuttons.
        self.radio_var = tkinter.IntVar()
        
        # Set the intVar object to 1.
        self.radio_var.set(1)

        # Create the Radiobutton widgets in the top_frame.
        #use lambda to combine 2 functions to use with each command
        self.rb1 = tkinter.Radiobutton(self.top_frame, \
                    text = 'Mammal', variable=self.radio_var, \
                    value = 1, command = lambda: [mammal.make_sound(), mammal.show_species()])

        self.rb2 = tkinter.Radiobutton(self.top_frame, \
                    text='Monkey', variable=self.radio_var, \
                    value = 2, command = lambda: [monkey.make_sound(), monkey.show_species()])
        
        self.rb3 = tkinter.Radiobutton(self.top_frame, \
                    text = 'Elephant', variable=self.radio_var, \
                    value = 3, command = lambda: [elephant.make_sound(), elephant.show_species()])

        self.rb4 = tkinter.Radiobutton(self.top_frame, \
                    text = 'Goat', variable=self.radio_var, \
                    value = 4, command = lambda: [goat.make_sound(), goat.show_species()])
         

        # Pack the Radiobuttons.
        self.rb1.pack()
        self.rb2.pack()
        self.rb3.pack()
        self.rb4.pack()
        # Create a Quit button to exit program.
 
        self.quit_button = tkinter.Button(self.bottom_frame, \
                      text='Quit', command = self.main_window.destroy)

        # Pack the Button.
 
        self.quit_button.pack(side = 'bottom')

        # Pack the frames.
        self.top_frame.pack()
        self.bottom_frame.pack()
        
        # Start the mainloop.
        tkinter.mainloop()



# Create an instance of the MyGUI class.
my_gui = MyGUI()
