# The Mammal class represents a generic mammal.
# the subprocess module allows us to playback sounds and use .gif 
import subprocess
class Mammal:

    # The __init__ method accepts an argument for
    # the mammal's species.

    def __init__(self, species, age, color):
        self.__species = species
        self.__age = age
        self.__color = color
    # The show_species method displays a message
    # indicating the mammal's species.

    def show_species(self):
        print('Species:I am a {}'.format(self.__species))
        print('')
        print('Age: My age is {}'.format(self.__age))
        print('')
        print('Color: My color is {}'.format(self.__color))
        print('')

                                          
        
    # The make_sound method is the mammal's
    # way of making a generic sound.

    def make_sound(self):
        cool_sound = "cool.wav"
        return_code = subprocess.call(["afplay", cool_sound])
        print('Grrrrr')
        print('')

# The Monkey class is a subclass of the Mammal class.

class Monkey(Mammal):

    # The __init__ method calls the superclass's
    # __init__ method passing 'Monkey' as the species.

    def __init__(self,species, age, color):
        Mammal.__init__(self, species, age, color) 
    # The make_sound method overrides the superclass's
    # make_sound method.

    def make_sound(self):
        monkey_sound = "Monkey.wav"
        return_code = subprocess.call(["afplay", monkey_sound])
        print('uh uh ah ah')

# The Elephant class is a subclass of the Mammal class.

class Elephant(Mammal):

    # The __init__ method calls the superclass's
    # __init__ method passing 'Elephant' as the species.

    def __init__(self, species, age, color):
       Mammal.__init__(self, species, age, color)
    # The make_sound method overrides the superclass's
    # make_sound method.

    def make_sound(self):
        elephant_sound = "Elephant.wav"
        return_code = subprocess.call(["afplay", elephant_sound])
        print('whatever a elephant sound makes')
class Goat(Mammal):
    def __init__(self, species, age, color):
        Mammal.__init__(self, species, age, color)
    def make_sound(self):
        goat_sound = "Goat.wav"
        return_code = subprocess.call(["afplay", goat_sound])
        print('bah')
           
