##################################################################
#  Daniel Serrano
#  Graph data structure
#                        
#   ***a router class will be created to store the names of the routers
#      dicitonary will be used to store key value pairs
#      where key = nodes(vertices)
#      value = connecting lines(edges)                     
#      Since there are multiple values being stored in the dictionary 
#      the values will be represented by a list to be able to store multiple
#      values per key
#                        
#                        
#                        
#####################################################################

class Router(object):

    def __init__(self, router_dict=None):
        """ constructor that creates a empty dictionary 
        """
        if router_dict == None:
            router_dict = {}
        self.__router_dict = router_dict

    def vertices(self):
        """ vertices will be stored as a list of keys """
        return list(self.__router_dict.keys())

    def edges(self):
        """ returns the edges of a graph """
        return self.__generate_edges()

    def add_vertex(self, vertex):
        """
            if the vertex(node) is not in the router dictionary a 
            key"router" will be created with an empty list 
            for values to be added to dictionary
        """
        if vertex not in self.__router_dict:
            self.__router_dict[vertex] = []

    def add_edge(self, edge):
       """vertices are connected by edges this method
          allows the vertices to have multiple edges
          or none at all
        """
       edge = set(edge)
       (vertex1, vertex2) = tuple(edge)
       if vertex1 in self.__router_dict:
           self.__router_dict[vertex1].append(vertex2)
       else:
           self.__router_dict[vertex1] = [vertex2]

    def __generate_edges(self):
        """ generate edges will connect the vertex to the neighbor vertex
        
        """
        edges = []
        #for loop to check router dictionary
        for vertex in self.__router_dict:
            #for loop that determines if vertex has edges 
            #if it does not append to the list
            for neighbour in self.__router_dict[vertex]:
                if {neighbour, vertex} not in edges:
                    edges.append({vertex, neighbour})
        return edges

    def __str__(self):
        """ to print out the graph"""
        res = "vertices: "
        for k in self.__router_dict:
            res += str(k) + " "
        res += "\nedges: "
        for edge in self.__generate_edges():
            res += str(edge) + " "
        return res

    def find_isolated_vertices(self):
        """ returns a list of isolated vertices. 
            determines if the graph has vertices connected
        """
        graph = self.__router_dict
        isolated = []
        for vertex in graph:
            print(isolated, vertex)
            if not graph[vertex]:
                isolated += [vertex]
        return isolated

    def find_path(self, start_vertex, end_vertex, path=[]):
        """ find a path from start_vertex to end_vertex 
            in graph """
        graph = self.__router_dict
        path = path + [start_vertex]
        #if statement that returns path if start == end
        if start_vertex == end_vertex:
            return path
        #if vertex has no edges return none
        if start_vertex not in graph:
            return None
        for vertex in graph[start_vertex]:
            if vertex not in path:
                extended_path = self.find_path(vertex, 
                                               end_vertex, 
                                               path)
                if extended_path: 
                    return extended_path
        return None
    

    def find_all_paths(self, start_vertex, end_vertex, path=[]):
        """ find all paths from start_vertex to 
            end_vertex in graph """
        graph = self.__router_dict 
        path = path + [start_vertex]
        if start_vertex == end_vertex:
            return [path]
        if start_vertex not in graph:
            return []
        paths = []
        for vertex in graph[start_vertex]:
            if vertex not in path:
                extended_paths = self.find_all_paths(vertex, 
                                                     end_vertex, 
                                                     path)
                for p in extended_paths: 
                    paths.append(p)
        return paths

    
    def vertex_adjacency(self, vertex):
        """method determines how many connections(edges) each router has""" 
        adj_vertices =  self.__router_dict[vertex]

        adj = len(adj_vertices) + adj_vertices.count(vertex)
        return adj

#start test 
if __name__ == "__main__":

    rout = { "asus" : ["dlink"],
          "belkin" : ["cisco"],
          "cisco" : ["belkin", "dlink", "cisco", "netgear"],
          "dlink" : ["asus", "cisco"],
          "netgear" : ["cisco"],
          "apple" : []
        }

    #graph will be populate by Objects from Router ^ rout
    graph = Router(rout)
    print(graph)

    #for loop to print out how many edges each vertex has
    for node in graph.vertices():
        print(graph.vertex_adjacency(node))

    #test print for isolated vertices
    #note the nodes inside 'here' will determine which are isolated 
    print("List of isolated vertices:")
    print(graph.find_isolated_vertices())

    #test to determine a path from one router to the next
    print("""A path from "belkin" to "netgear":""")
    print(graph.find_path("belkin", "netgear"))

    #test to determine all paths the data must take to reach destination
    #beginning to end
    print("""All pathes from "asus" to "netgear":""")
    print(graph.find_all_paths("asus", "netgear"))

    #test to print edges of graph
    print("Edges:")
    print(graph.edges())



    
    #test to add edge to new nodes
    print("Add edge ('palo alto','synology'): ")
    graph.add_edge(('palo alto', 'synology'))
    print(graph)

    #test to add an edge between unconnected vertexes
    print("Add edge ('asus','apple'): ")
    graph.add_edge(('asus', 'apple'))
    print(graph)

    #test that proves apple is no longer an isolated vertex
    print(graph.find_isolated_vertices())

    
