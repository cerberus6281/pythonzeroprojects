from router import Router

def main():

    #router list to populate router graph
    router_list =  {"Apple" : ["Belkin"],
                    "Belkin": ["Apple"],
                    "Cisco" : ["Dlink","Netgear","Palo Alto","Apple","Belkin","Asus"],
                    "Dlink" : ["Apple","Netgear"],
                    "Asus"  : []
                }
    
    router_graph = Router(router_list)

    print(router_graph)

    #a new vertex will be added
    router_graph.add_vertex("Motorola")

    #a edge will be created between Asus and Dlink
    router_graph.add_edge(("Dlink","Palo Alto"))

    #for loop to print how many edges each vertix has
    for node in router_graph.vertices():
        print(router_graph.vertex_adjacency(node))

    #print updated graph
    print(router_graph)

    #print isolated vertices
    print(router_graph.find_isolated_vertices())
    #isolated vertices will be flagged with quotes 

    path = router_graph.find_all_paths("Cisco","Belkin")
    print(path)

   
    
#call main
main()    


